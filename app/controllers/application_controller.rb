# We need to test merging and force pushes, so let's do that here.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def hello
    render text: "hello, world!"
  end

  def goodbye
    Rails.logger.info("Wow! Cool!")
    render text: "goodbye, cruel world!"
  end
end
